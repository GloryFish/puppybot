import RPi.GPIO as GPIO


def delay(j):
    for k in range(1, j):
        pass


class Speaker():

    def __init__(self):
        self.speaker = 22
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.speaker, GPIO.OUT)

    def beep(self):
        for j in range(1100, 1, -1):
            GPIO.output(self.speaker, GPIO.HIGH)
            delay(j)
            GPIO.output(self.speaker, GPIO.LOW)
            delay(j)
