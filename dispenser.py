import time
import logging

from Phidgets.Devices.AdvancedServo import AdvancedServo
from Phidgets.Devices.Servo import ServoTypes
from Phidgets.PhidgetException import PhidgetException


class Dispenser():
    """Represents the treat dispenser."""

    def __init__(self, logname):
        self.servo = AdvancedServo()
        self.servo.openPhidget()
        self.logger = logging.getLogger(logname)

    def start(self):
        """Connect servos and set defaults."""
        try:
            self.servo.waitForAttach(5000)
        except PhidgetException as e:
            self.logger.exception('%i: %s' % (e.code, e.details))
            return

        try:
            self.servo.setServoType(0, ServoTypes.PHIDGET_SERVO_HITEC_HS422)
            self.servo.setServoType(1, ServoTypes.PHIDGET_SERVO_HITEC_HS422)
            self.servo.setServoType(2, ServoTypes.PHIDGET_SERVO_HITEC_HS422)
            self.servo.setServoType(3, ServoTypes.PHIDGET_SERVO_HITEC_HS422)
        except PhidgetException as e:
            self.logger.exception('%i: %s' % (e.code, e.details))
            return

        self.logger.info('Dispenser started')

    def set_servo(self, servo, value):
        """Set an attached servo to a specific position.

        'servo' is the Phidgets AdvancedServo servo number.

        """

        self.servo.setEngaged(servo, True)
        self.servo.setPosition(servo, value)

    def give_treat(self):
        """Give the doggies a treat!"""
        self.servo.setEngaged(0, True)
        self.servo.setPosition(0, 180)
        time.sleep(1.5)

        self.servo.setPosition(0, 0)
        time.sleep(1.5)

        self.servo.setEngaged(0, False)

    def stop(self):
        """Disengage servos."""
        self.servo.setEngaged(0, False)
        self.servo.setEngaged(1, False)
        self.servo.setEngaged(2, False)
        self.servo.setEngaged(3, False)

        self.servo.closePhidget()
