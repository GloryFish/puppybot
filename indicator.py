import RPi.GPIO as GPIO


class Indicator():
    """Represents a set of red and green LEDS used to indicate the status of the treat dispenser.

    This module assumes that general GPIO configuration (i.e. GPIO.setmode()) has been done before start() is called.

    """

    def __init__(self, redchannel, greenchannel):
        """redchannel and greenchannel are the GPIO channels where the matching LEDs are connected."""

        self.red = redchannel
        self.green = greenchannel

    def start(self):
        """Sets up GPIO pin output for the LEDs"""

        GPIO.setup(self.green, GPIO.OUT)
        GPIO.setup(self.red, GPIO.OUT)

    def all_on(self):
        GPIO.output(self.green, GPIO.HIGH)
        GPIO.output(self.red, GPIO.HIGH)

    def all_off(self):
        GPIO.output(self.green, GPIO.LOW)
        GPIO.output(self.red, GPIO.LOW)

    def red_on(self):
        GPIO.output(self.green, GPIO.LOW)
        GPIO.output(self.red, GPIO.HIGH)

    def green_on(self):
        GPIO.output(self.green, GPIO.HIGH)
        GPIO.output(self.red, GPIO.LOW)

    def stop(self):
        """Turns out the lights."""

        self.all_off()
