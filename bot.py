#! /usr/bin/python

import cmd
import logging
import time

import RPi.GPIO as GPIO

import dispenser
import indicator
import speaker


class BotCommander(cmd.Cmd):
    """Command processor for the puppy bot."""

    def preloop(self):
        """Hook method executed once when cmdloop() is called.

        Sets up some initial settings.

        """

        self.intro = 'PuppyBot v2.0'
        self.prompt = '> '

        # Configure logging
        logging.basicConfig(level=logging.INFO)
        self.logger = logging.getLogger('puppybot')

        # Configure RPi.GPIO
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BOARD)

        # Physical treat dispenser
        self.dispenser = dispenser.Dispenser('puppybot')
        self.dispenser.start()

        # Indicator lights
        self.indicator = indicator.Indicator(redchannel=18, greenchannel=16)
        self.indicator.start()
        self.indicator.green_on()

        # Speaker
        self.speaker = speaker.Speaker()

    def do_beep(self, line):
        self.speaker.beep()

    def do_treat(self, line):
        """Trigger the treat dispenser and update indicator lights."""

        self.indicator.red_on()
        self.speaker.beep()
        self.dispenser.give_treat()
        self.indicator.green_on()

    def do_setservo(self, line):
        """Set a servo to a specific value.

        Syntax: setservo [number] [value]

        """

        args = line.split()
        servo = int(args[0])
        value = int(args[1])

        self.logger.info('servo: %i, value: %i', servo, value)

        self.dispenser.set_servo(servo, value)

    def do_blink(self, line):
        """Blink all lights on and off for testing"""

        self.indicator.all_on()
        time.sleep(1)
        self.indicator.all_off()
        time.sleep(1)
        self.indicator.all_on()
        time.sleep(1)
        self.indicator.all_off()

    def do_exit(self, line):
        """Quit PuppyBot, safely deactivating the system."""
        return self.quit()

    def help_exit(self):
        print "Quit PuppyBot, safely deactivating the system."

    def do_EOF(self, line):
        return self.quit()

    def quit(self):
        """Shut down the bot cleanly and clean up external resources."""
        self.dispenser.stop()
        self.indicator.stop()
        GPIO.cleanup()
        return True

if __name__ == '__main__':
    BotCommander().cmdloop()
